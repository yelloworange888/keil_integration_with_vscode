if not exist "%PRODUCTION_FOLDER%" (
  echo mkdir "%PRODUCTION_FOLDER%"
  mkdir "%PRODUCTION_FOLDER%"
)
if not exist "%TMP_FOLDER%" (
  echo mkdir "%TMP_FOLDER%"
  mkdir "%TMP_FOLDER%"
)
