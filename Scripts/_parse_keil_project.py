import argparse
import xml.etree.ElementTree as ET
import json

additional_paths_for_all_targets = [ "C:/Keil_v5/ARM/ARMCLANG/include",\
                                     "C:/Keil_v5/ARM/ARMCC/include"]

class DATA():
    def __init__(self, target_name, include_path, define, outputDir, listingDir, optimLevel):
        self.target_name = target_name
        self.include_path = include_path
        self.define = define
        self.outputDir = outputDir
        self.listingDir = listingDir
        self.optimLevel = optimLevel

def read_keil_v5_xml_file(fullFilePath, list):
    tree = ET.parse(fullFilePath)
    root = tree.getroot()
    for target in root.iter('Target'):
        target_name = target.find('TargetName').text
        include_path = target.find('TargetOption/TargetArmAds/Cads/VariousControls/IncludePath').text
        define = target.find('TargetOption/TargetArmAds/Cads/VariousControls/Define').text
        outputDir = target.find('TargetOption/TargetCommonOption/OutputDirectory').text
        listingDir = target.find('TargetOption/TargetCommonOption/ListingPath').text
        optimLevel = target.find('TargetOption/TargetArmAds/Cads/Optim').text
        list.append(DATA(target_name, include_path.split(';'), define.split(','), outputDir, listingDir, optimLevel))

def read_keil_v4_xml_file(fullFilePath, list):
    global iSkipEverySecondTarget
    iSkipEverySecondTarget = 0
    tree = ET.parse(fullFilePath)
    root = tree.getroot()
    for target in root.iter('Target'):
        iSkipEverySecondTarget += 1
        if (iSkipEverySecondTarget % 2 == 0):
            continue
        target_name = target.find('TargetName').text
        include_path = target.find('TargetOption/TargetArmAds/Cads/VariousControls/IncludePath').text
        define = target.find('TargetOption/TargetArmAds/Cads/VariousControls/Define').text
        outputDir = target.find('TargetOption/TargetCommonOption/OutputDirectory').text
        listingDir = target.find('TargetOption/TargetCommonOption/ListingPath').text
        optimLevel = target.find('TargetOption/TargetArmAds/Cads/Optim').text
        list.append(DATA(target_name, include_path.split(';'), define.split(','), outputDir, listingDir, optimLevel))

def ConvertToVsCodeFormat(KeilProjectPath, list):
    for l in list:
        for n, i in enumerate(l.include_path):
            l.include_path[n] = i.replace("\\\\", "/")
            l.include_path[n] = i.replace("\\", "/")
            if KeilProjectPath is None:
                l.include_path[n] = "${workspaceFolder}/" + l.include_path[n]
            else:
                l.include_path[n] = "${workspaceFolder}/" + KeilProjectPath + "/" + l.include_path[n]
        for a in additional_paths_for_all_targets:
            l.include_path.insert(0, a)
    return list

def WriteJsonFile(list, path):
    text = "{\n  \"configurations\": [\n"
    for list_cnt, list_ele in enumerate(list):
        text += "    {\n"
        text += "      \"name\": \"" + list_ele.target_name + "\",\n"
        text +=  "      \"includePath\": [\n"
        for cnt, ele in enumerate(list_ele.include_path):
            text += "        \"" + ele + "\""
            if(cnt == len(list_ele.include_path)-1):
                text += "\n"
            else:
                text += ",\n"
        text += "      ],\n"
        text += "        \"defines\": [\n"
        for cnt, ele in enumerate(list_ele.define):
            text += "        \"" + ele.replace(" ", "") + "\""
            if(cnt == len(list_ele.define)-1):
                text += "\n"
            else:
                text += ",\n"
        text += "      ]\n"
        if(list_cnt == len(list)-1):
            text += "    }\n"
        else:
            text += "    },\n"
    text += "  ],\n  \"version\": 4\n}"
    #print(text)
    
    json_file = open(path, "w")
    json_file.write(text)
    json_file.close()
    print("Done.")
    
def WriteParseKeilProjectBat(KeilProjectPath, KeilProjectFilename):
    if KeilProjectPath is None:
        text = f'SET KEIL_PROJECT_PATH={KeilProjectFilename}\n'
    else:
        text = f'SET KEIL_PROJECT_PATH={KeilProjectPath}/{KeilProjectFilename}\n'
    text += f'python3 ./scripts/_parse_Keil_project.py %KEIL_PROJECT_PATH% .vscode/c_cpp_properties.json ./scripts/_set_Keil_project_paths.bat\n'
    text += f'call scripts\\_set_Keil_project_paths.bat'

    batch_file = open("./scripts/_parse_Keil_project.bat", "w")
    batch_file.write(text)
    batch_file.close()

def WriteKeilProjectPathBatchFile(KeilProjectPath, KeilProjectFilename, bat_file_path, list):
    if KeilProjectPath is None:
        text = "SET PROJECT_FOLDER=.\n"
    else:
        text = "SET PROJECT_FOLDER=" + KeilProjectPath + "\n"
    text += "SET KEIL_PROJECT_NAME_UVPROJX=" + KeilProjectFilename + "\n"
    text += "\n"

    text += "SET PRODUCTION_FOLDER=production\n"
    text += "SET TMP_FOLDER=ci_tmp\n"
    text += "SET BUILD_LOG=build.log\n"
    text += "\n"

    index = 0
    for l in list:
        text += "IF \"%PROJECT_TARGET%\"==\"" + l.target_name + "\" (\n"
        text += "\tSET KEIL_PROJECT_OUTPUT_DIR=" + l.outputDir + "\n"
        text += "\tSET KEIL_PROJECT_LISTING_DIR=" + l.listingDir + "\n"
        text += "\tSET KEIL_PROJECT_OPTIM_LEVEL=" + l.optimLevel + "\n"
        text += ")\n"
        text += "SET PROJECT_TARGET_LIST[" + str(index) + "]=" + str(l.target_name) +"\n"
        text += "SET KEIL_PROJECT_OUTPUT_DIR_LIST[" + str(index) + "]=" + str(l.outputDir) +"\n"
        text += "SET KEIL_PROJECT_LISTING_DIR_LIST[" + str(index) + "]=" + str(l.listingDir) +"\n"
        text += "SET KEIL_PROJECT_OPTIM_LEVEL_LIST[" + str(index) + "]=" + str(l.optimLevel) +"\n"
        index += 1
        text += "\n"

    text += "SET PROJECT_TARGET_LIST_LENGTH=" + str(index - 1) +"\n"

    batch_file = open(bat_file_path, "w")
    batch_file.write(text)
    batch_file.close()

def PrintData(list):
    for l in list:
        print("Target name:", l.target_name)
        print("  Include_path:")
        for e in l.include_path:
            print("    ", e)
        print("  Define:")
        for e in l.define:
            print("    ", e)

if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('xml_file_path')
    PARSER.add_argument('json_file_path')
    PARSER.add_argument('bat_file_path')
    APP_ARGS = PARSER.parse_args()
    APP_ARGS.xml_file_path = APP_ARGS.xml_file_path.replace("\\", "/")
    
    path_Filename = APP_ARGS.xml_file_path.rsplit("/", 1)
    if len(path_Filename) > 1:
        KeilProjectPath = path_Filename[0]
        KeilProjectFilename = path_Filename[1]
    else:
        KeilProjectPath = None
        KeilProjectFilename = path_Filename[0]
    
    list_xml = []
    
    print(f'Parsing Keil\'s project: "{APP_ARGS.xml_file_path}"')
    if APP_ARGS.xml_file_path.endswith('.uvproj'):
        read_keil_v4_xml_file(APP_ARGS.xml_file_path, list_xml)
    elif APP_ARGS.xml_file_path.endswith('.uvprojx'):
        read_keil_v5_xml_file(APP_ARGS.xml_file_path, list_xml)
        
    list_xml = ConvertToVsCodeFormat(KeilProjectPath, list_xml)
    #PrintData(list_xml)
    WriteKeilProjectPathBatchFile(KeilProjectPath, KeilProjectFilename, APP_ARGS.bat_file_path, list_xml)
    WriteParseKeilProjectBat(KeilProjectPath, KeilProjectFilename)
    WriteJsonFile(list_xml, APP_ARGS.json_file_path)
