@echo off

echo(
echo PROJECT_FOLDER: "%PROJECT_FOLDER%"
echo KEIL_PROJECT_NAME_UVPROJX: "%KEIL_PROJECT_NAME_UVPROJX%"
echo BUILD_LOG: "%BUILD_LOG%"

call Scripts\_check_validity.bat

for %%I in ("%PROJECT_FOLDER%\%KEIL_PROJECT_NAME_UVPROJX%") do (
  echo UV4.exe -c -j0 %%I -o "%BUILD_LOG%"
  echo(
  UV4.exe -c -j0 %%I -o "%BUILD_LOG%"
  IF EXIST "%PROJECT_FOLDER%\%BUILD_LOG%" type "%PROJECT_FOLDER%\%BUILD_LOG%"
)
