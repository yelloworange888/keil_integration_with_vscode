@echo off

rem Keil (UV4.exe) & fromelf.exe must be on PATH

setlocal enabledelayedexpansion

call Scripts\_parse_Keil_project.bat
call Scripts\_check_validity.bat

echo(
if exist "Scripts\_increment_version.bat" (
  call Scripts\_increment_version.bat
)

echo(
echo The project for ALL configurations is cleaning. Please wait...
call Scripts\_create_folders.bat
break>"%TMP_FOLDER%\program_size_summary.log"

call Scripts\clean.bat
echo(
echo Done.
