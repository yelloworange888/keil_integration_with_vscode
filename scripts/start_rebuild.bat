@echo off

rem Keil (UV4.exe) & fromelf.exe must be on PATH

setlocal enabledelayedexpansion

IF "%~1"=="" (
  SET PROJECT_TARGET=Release
) ELSE (
  SET PROJECT_TARGET=%1
)

call Scripts\_parse_Keil_project.bat
call Scripts\_check_validity.bat
call Scripts\_create_folders.bat
call Scripts\_create_folders_for_target.bat

call Scripts\rebuild.bat
call Scripts\convert.bat

echo(
echo %PROJECT_TARGET% rebuilding is done.
