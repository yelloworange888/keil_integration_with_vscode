@echo off

echo(
echo The project for configuration "%PROJECT_TARGET%" is converting. Please wait...
echo PROJECT_FOLDER: "%PROJECT_FOLDER%"
echo PRODUCTION_FOLDER: "%PRODUCTION_FOLDER%"
echo TMP_FOLDER: "%TMP_FOLDER%"
echo PROJECT_TARGET: "%PROJECT_TARGET%"

call Scripts\_check_validity.bat

for %%I in ("%PROJECT_FOLDER%\%KEIL_PROJECT_OUTPUT_DIR%*.axf") do (
  echo fromelf.exe "%%I" --bin --output "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin"
  fromelf.exe "%%I" --bin --output "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin"
  IF %ERRORLEVEL% NEQ 0 (
    echo Fromelf returned %ERRORLEVEL%
    EXIT %ERRORLEVEL%
  )
  
  echo fromelf.exe "%%I" --elf --output "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.elf"
  fromelf.exe "%%I" --elf --output "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.elf"
  IF %ERRORLEVEL% NEQ 0 (
    echo Fromelf returned %ERRORLEVEL%
    EXIT %ERRORLEVEL%
  )
  
  echo copy "%%~dpnI.hex" "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.hex"
  copy "%%~dpnI.hex" "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.hex"
  echo copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%TMP_FOLDER%\%PROJECT_TARGET%\_app.raw_bin"
  copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%TMP_FOLDER%\%PROJECT_TARGET%\_app.raw_bin"

  if /i "!PROJECT_TARGET:~0,5!"=="[REL]" (
    echo copy "%%~dpnI.hex" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\%%~nI.hex"
    copy "%%~dpnI.hex" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\%%~nI.hex"
    echo copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\%%~nI.bin"
    copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\%%~nI.bin"
    echo copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\_app.raw_bin"
    copy "%TMP_FOLDER%\%PROJECT_TARGET%\%%~nI.bin" "%PRODUCTION_FOLDER%\%PROJECT_TARGET%\_app.raw_bin"
  )

  echo(
  call Scripts\_summary.bat "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%%%~nI.map:     Code (inc. data)    RO Data    RW Data    ZI Data      Debug"
)
