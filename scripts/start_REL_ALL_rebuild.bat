@echo off

rem Keil (UV4.exe) & fromelf.exe must be on PATH

setlocal enabledelayedexpansion

call Scripts\_parse_Keil_project.bat
call Scripts\_check_validity.bat

echo(
if exist "Scripts\_increment_version.bat" (
  call Scripts\_increment_version.bat
)

echo(
echo The project for ALL [REL] configurations is rebuilding. Please wait...
call Scripts\_create_folders.bat
break>"%TMP_FOLDER%\program_size_summary.log"

for /l %%I in (0,1,%PROJECT_TARGET_LIST_LENGTH%) do ( 
  if /i "!PROJECT_TARGET_LIST[%%I]:~0,5!"=="[REL]" (
    echo(
    echo === !PROJECT_TARGET_LIST[%%I]! ===
    echo call Scripts\start_rebuild.bat !PROJECT_TARGET_LIST[%%I]!
    call Scripts\start_rebuild.bat !PROJECT_TARGET_LIST[%%I]!
  )
)

echo(
echo The flashers for ALL configurations are building. Please wait...
call Scripts\make_ALL_flashers.bat

echo(
echo ALL [REL] Done.
call Scripts\_beeps_at_the_end.bat
